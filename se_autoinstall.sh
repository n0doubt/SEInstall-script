#!/bin/bash
# file: se_autoinstall.sh
echo "RUN THIS AS ROOT RUN THIS AS ROOT RUN THIS AS ROOT RUN THIS AS ROOT RUN THIS AS ROOT"
read -p "Press enter to continue"
#
#
clear
echo -e "\n""\n""\n""\n""\n""--UPDATING SYSTEM PACKAGES AND INSTALLING DEPENDENCIES--""\n""\n""\n""\n""\n"
apt-get update && apt-get upgrade # AY YO UPGRADE
apt-get install build-essential wget -y
#
#
clear
echo -e "\n""\n""\n""\n""\n""\aUPDATES FINISHED, IT IS RECOMENDED THAT YOU REBOOT IF THIS IS A FRESH SYSTEM AND RUN THIS SCRIPT AGAIN\n""\n""--INSTALLING SOFTETHER VPN SERVER v4.28-9669-beta-2018.09.11-linux-x64-64bit4--""\n""\n""\n""\n""\n"
read -p "Press enter to continue"
#
#
wget http://www.softether-download.com/files/softether/v4.28-9669-beta-2018.09.11-tree/Linux/SoftEther_VPN_Server/64bit_-_Intel_x64_or_AMD64/softether-vpnserver-v4.28-9669-beta-2018.09.11-linux-x64-64bit.tar.gz
tar xzvf softether-vpnserver-v4.28-9669-beta-2018.09.11-linux-x64-64bit.tar.gz
cd vpnserver
#
#
echo -e "\n""\n""\n""\n""\n""\n""\n""\n""THIS SCRIPT WILL NOW RUN THE MAKE COMMAND IN THE VPN SERVER DIRECTORY \n""PLEASE AGREE TO ALL LICENCE AGREEMENTS BELOW, THIS SCRIPT WILL CONTINUE THE INSTALLATION PROCESS AUTOMATICALLY AFTER MAKE\n""\n"
read -p "Press enter to continue"
make
#
#
echo -e "\n""\n""\n""\n""\aIF YOU SEE ANY MAKE ERRORS IN THE OUTPUT ABOVE, PLEASE PRESS CTRL+C AND FIX THEM!\n""OTHERWISE THIS SCRIPT WILL NOW COPY THE VPN SERVER FILES TO THEIR NEW HOME IN /usr/local/vpnserver""\n""\n""\n""\n"
read -p "Press enter to continue"
#
#
cd ..
mv vpnserver /usr/local
mv vpnserver.autostart /etc/init.d/vpnserver
#
#
echo -e "\n""\n""--CHANGING FILE PERMISSIONS TO MAKE EVERYTHING WORK!--\n""\n"
chmod 600 /usr/local/*
chmod 700 /usr/local/vpnserver/vpnserver
chmod 700 /usr/local/vpnserver/vpncmd
chmod 755 /etc/init.d/vpnserver
echo -e "\n""\n""\n""\n""\n""\n""\n""\n""DOUBLE CHECK FOR PERMISSION CHANGE ERRORS ABOVE, IF THERE ARE THIS INSTALLATION HAS FAILED\n""\n"
read -p "Press enter to continue"
#
#
echo -e "\n""\n""--STARTING NEW VPN SERVICE AND UPDATING rc.d defaults--\n""\n"
/etc/init.d/vpnserver start
update-rc.d vpnserver defaults
#
#
# vpncmd automation goes here
service vpnserver restart
service vpnserver status
touch /etc/motd
echo -e "--To access vpncmd, cd to /usr/local/vpnserver and type './vpncmd'\n""-- SoftEther VPN server should now be registered as as service and start automatically at boot. To start vpnserver, type 'service vpnserver start' as root or add 'sudo' to the previous command." > /etc/motd
echo -e "\n""\n""\n""\n""\n""\n""\n""\n""\aFINISHED!""\n""\n""1. To access vpncmd, cd to /usr/local/vpnserver and type './vpncmd'\n""2. SoftEther VPN server should now be registered as as service and start automatically at boot. To start vpnserver, type 'service vpnserver start' as root or add 'sudo' to the previous command.\n""3. These notes have been copied to '/etc/motd'.""\n""\n"
#
#
exit 0
#